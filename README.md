
# Overview
This repository has two main directories:

- ``workflow`` which contains the experimental workflow used to generate the data.
- ``analysis`` which contains the research report and the notebook used of visualization

## Installation
First, install [nix](https://nixos.org/nix/) package manager

Then, clone this nix repository which contains all the packages need to run the experiments.
```{sh} 
git clone https://gitlab.inria.fr/szrigui/mixed-policies.git
```

## Experiments:
``Loading the nix environment``: The following command will install all the software and the dependencies needed the run the experiments
There two main experiment can be run by executing:
```{sh} 
nix-shell -A zymakefile_bbo:
```
The two [zymake](http://www-personal.umich.edu/~ebreck/code/zymake/) files contain the workflows to run the two main experiments.

learn all the mixed policies and simulated the pure and the mixed policies and save the results:
```{sh}  
zymake zymakefile_comparing_all
```
simulate and get the space coverage experiments
```{sh} 
zymake zymakefile_covering_space
```
to run the space coverage experiments

The parameters can be found at the beginning of the tow files 

Parameters:

- ``traces``: the traces that are used in the experimental campaign
- ``period (second)``: variable used in the separation of the trace. The trace will be divided into parts of size “period”
- ``back``: the backfilling policies (fixed to SPF)
- ``stat``: the metric the experiment will use. (In this work we use only the bounded slowdown)
- ``budget``: the maximum number of steps of xnes algorithm is allowed to run for.
- ``id_train``: the weeks that we want the user for the experiment: range (min max). 



## Visualization
The data used to produce the main article and the research report is stored in a folder called data
All the Figures in the main article and in the research report can be produced using these four notebooks: 

- [SDSC-SP2](analysis/visualization_notebook_SDSC-SP2.pdf)
- [SDSC-BLUE](analysis/visualization_notebook_SDSC-BLUE.pdf)
- [KTH-SP2](analysis/visualization_notebook_KTH-SP2.pdf)
- [CTC-SP2](analysis/visualization_notebook_CTC_SP2.pdf)




