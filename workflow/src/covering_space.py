#!/usr/bin/env python3
# encoding: utf-8

'''
    bbo

Usage:
    bbo.py <n> <more_arg> <norm_file> <trace> <threshold> <vec_out> <swf_file>... [-h]

Options:
    <n>            budget
    <swf_file>     an input file
    <more_arg>     extra cli ocs arguments
    <trace>        name of the current trace
    <threshold>    threshold value
    <vec_out>      output vector
    <norm_file>    normalizer file
    -h --help      show this help message and exit.
'''


from docopt import docopt
import pybrain
import subprocess
import os
import time
import pandas as pd
import csv
import numpy as np
import math
import pybrain.optimization as opt
import csv




def v2s(v):
	return(','.join([str(e) for e in v]))

def makev(x,norm,a,b):
	return("%s:%s:%s" %(v2s(x[a:b]),v2s(norm[0][a:b]),v2s(norm[1][a:b])))


def simulate(f,x):
	x=[x[0],x[1],x[2],0,0,0]
	s=["ocs","mixed",f,"--alpha=%s" %makev(x,norm,0,len(x)),"--threshold="+threshold]+more_arg
	s=" ".join(s)
	p=subprocess.Popen(s,stdout=subprocess.PIPE,shell=True)
	if p.poll() is None:
		p.wait()
	obj=float(p.stdout.read())
	return obj
def simulate_max(f,x):

	x=[x[0],x[1],x[2],0,0,0]
	s=["ocs","mixed",f,"--alpha=%s" %makev(x,norm,0,len(x)),"--threshold="+threshold,'--backfill=spf','--stat=maxbsld']
	s=" ".join(s)
	p=subprocess.Popen(s,stdout=subprocess.PIPE,shell=True)
	if p.poll() is None:
		p.wait()
	obj=float(p.stdout.read())
	return obj


def generate_space_table():
	sigs=[[1,1,1],[-1,1,1],[1,-1,1],[1,1,-1],[-1,-1,1],[-1,1,-1],[1,-1,-1],[-1,-1,-1]]
	tab=[]   
	for sig in sigs:
		for e1 in range(0,101):
			for e2 in range(e1,101):
				l=[e1/100,(100-e2)/100,(-e1+e2)/100]
			tab.append([a*b for a,b in zip(l,sig)])
	return tab



def save_progress(l=[],file_name="logs/place_holder.csv",mode='a'):
	with open(file_name, mode) as file_temp:
		writer = csv.writer(file_temp)
		writer.writerows(l)



if __name__ == '__main__':
	args = docopt(__doc__, version='1.0.0rc2')
	with open(args["<norm_file>"]) as f:
		norm = [[float(e) for e in l.split(",")]  for l in f]
	with open(args["<more_arg>"]) as f:
		more_arg = [l.strip() for l in f]
	threshold= args["<threshold>"].strip()
	trace=args["<trace>"].strip()


	save_count_down=100
	space_table=generate_space_table()
	grand_list=[]
	grand_list.append(["id","policy","avg","max"])
	grand_list=[]

	save_progress(l=grand_list,file_name="logs/"+trace+"_covering_space.csv",mode="w")


	for f in args["<swf_file>"]:
		for v in space_table:
			policy_name=",".join(str(e) for e in v)
			l=[f[-25:],policy_name,simulate(f,v),simulate_max(f,v)]
			print(l)
			grand_list.append(l)
			save_count_down-=1
			if(save_count_down<=0):
				save_progress(l=grand_list,file_name="logs/"+trace+"_covering_space.csv",mode="a")
				grand_list=[]
				save_count_down=100

