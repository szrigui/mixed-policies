---
title: 'ANL-Intr'
output:
  pdf_document: default
  html_notebook: default
---
```{r global_options, include=FALSE}
knitr::opts_chunk$set(fig.width=20 ,fig.height=8 ,
                      echo=FALSE, warning=FALSE, message=FALSE)
```
```{r fig.width=20,fig.height=8 } 
library(ggplot2)
library(reshape2)
library(dplyr)
library(viridis)
library(scales)
library(tidyr)
library(ggrepel)#for the labelling on the image
library(data.table)
library(directlabels)
library(RColorBrewer)
library(xtable)
```

```{r fig.width=20 ,fig.height=8 } 
read_trace<-function(path ,case=0){  
  d=read.csv(path,header = TRUE)

  d$policy=recode(d$policy,"fcfs"="FCFS")
  d$policy=recode(d$policy,"lcfs"="LCFS")
  
  d$policy=recode(d$policy,"saf"="SAF")
  d$policy=recode(d$policy,"laf"="LAF")

  d$policy=recode(d$policy,"lpf"="LPF")
  d$policy=recode(d$policy,"spf"="SPF")
  
  d$policy=recode(d$policy,"sqf"="SQF")
  d$policy=recode(d$policy,"lqf"="LQF")
  
  d$policy=recode(d$policy,"srf"="SRF")
  d$policy=recode(d$policy,"lrf"="LRF")
    
  d$policy=recode(d$policy,"sexp"="SEXP")
  d$policy=recode(d$policy,"lexp"="LEXP")
  
  return(d)
}
```
## path to the trace:
```{r}
performance_path="data/ANL-Intr/policies_performace.csv"
save_policies_name="ANL-Intr"
case=0
```
## comparing SAF with the minimum
```{r fig.width=20 ,fig.height=8}

d=read_trace(performance_path,case = case)
  
d=d %>% filter(policy %in% c("SAF","LAF","SPF","LPF","FCFS","LCFS","SRF","LRF","SEXP","LEXP","SQF","LQF"))
d_min=d %>% group_by(id) %>%filter(avg==min(avg))
d_min$policy="Best_policy"
d_SAF=d %>% group_by(id) %>%filter(policy=="SAF")
d_min_vs_saf=rbind(d_min,d_SAF)

ggplot(d_min_vs_saf)+
  geom_line(aes(x=id,y=avg,col=policy,lty=policy))+
  theme_bw()+
  ylim(0,15)+
  scale_linetype_manual(values=c(Best_policy="solid", SAF="dashed"))+
  scale_color_manual(values=c(Best_policy="#F8766D",SAF="#00BFC4"))+
  theme(plot.title = element_text(size = 25, face = "bold"),
            axis.text = element_text(size = 15),
            axis.title.x = element_text(size = 25),
            axis.title.y = element_text(size = 25),
            legend.text = element_text(size=25),
            legend.title = element_text(size=25),
            legend.position = "none"
          )+
  
   geom_label_repel(
      arrow = arrow(length = unit(0.015, "npc"), type = "closed", ends = "last"),
      force = 5,
      data=subset(d_min_vs_saf,id==3 & avg==subset(d_min_vs_saf,id==3 & policy=='SAF')$avg),
      label="SAF",
      x=3,y=subset(d_min_vs_saf,id==3 & policy=='SAF')$avg,
      ylim=c(13,13), xlim=c(2,2), color="#00BFC4",size=7
    )+
  geom_label_repel(
      arrow = arrow(length = unit(0.015, "npc"), type = "closed", ends = "last"),
      force = 5,
      data=subset(d_min_vs_saf,id==3 & avg==subset(d_min_vs_saf,id==3 & policy=='Best_policy')$avg),
      label="Best policy",
      x=3,y=subset(d_min_vs_saf,id==3 & policy=='Best_policy')$avg,
      ylim=c(0,0), xlim=c(2,2), color="#F8766D",size=7
    )+
  
  xlab("week")+
  ylab("average bounded slowdown")+
    scale_x_continuous(breaks=seq(0,100,2))+
  ggsave(paste("images/minPure_vs_saf_",save_policies_name,".pdf",sep=""), width=20,height=8)

```

## comparing best pure polices with learned policies:
```{r fig.width=20 ,fig.height=8}
d=read_trace(performance_path,case=case)
d$policy=recode(d$policy,"past_week_3"="w_greedy")
d$policy=recode(d$policy,"xnes_dynamic_clairvoyant_3"="w")
d$policy=recode(d$policy,"learning_offline_3"="w_train")
display_list=c("SAF","FCFS","w","w_train","w_greedy")
d_filterd=d %>%filter(policy %in% display_list )
d_filtred_temp=d_filterd %>% filter(policy != "w_train")
d_min_absolut=d_filtred_temp %>%group_by(id)%>% summarise(minimum=min(avg))
d_max_absolut=d_filtred_temp%>%group_by(id)%>% summarise(maximum=max(avg))
d_filtred_temp$mid=(d_max_absolut$maximum+d_min_absolut$minimum)/2
d_filtred_temp$width=(d_max_absolut$maximum-d_min_absolut$minimum)/2


color_list=c("gray42","skyblue1","magenta","green")
p=ggplot(d_filterd)+
  geom_ribbon(data=d_filtred_temp,aes(x=id,ymin = mid - width, ymax = mid + width), fill = "grey90") +
  geom_line(aes(x=id,y=avg,col=policy,lty=policy))+
  scale_linetype_manual(values=c( FCFS="dotted",SAF="solid", w="dotted",w_train="solid",w_greedy="solid"))+
  scale_color_manual(values=c(FCFS="#F8766D",w_train="#C77CFF",w_greedy="#000000",SAF="#00BFC4",w="#7CAE00"))+
  theme(axis.text=element_text(size=15),legend.text=element_text(size=15),axis.title=element_text(size=15))+
  geom_rect(mapping = aes(xmin=0,xmax=22.2,ymin=0,ymax=30),alpha=0.0015,color="black",linetype=2,size=0.2)+
  geom_rect(mapping = aes(xmin=22.8,xmax=45,ymin=0,ymax=30),alpha=0.0015,color="black",linetype=2,size=0.2)+
  annotate("text",x=11,y=28,label='atop(bold("Training"))',parse=TRUE,size=7)+
  annotate("text",x=33,y=28,label='atop(bold("Testing"))',parse=TRUE,size=7)+
  scale_x_continuous(breaks=seq(0,100,2))+
  theme(legend.position = "none")+
  geom_label_repel(
    arrow = arrow(length = unit(0.015, "npc"), type = "closed", ends = "last"),
    force = 5,
    data=subset(d_filterd,id==13 & avg==subset(d_filterd,id==13 & policy=='FCFS')$avg),
    label="FCFS",
    x=13,y=subset(d_filterd,id==13 & policy=='FCFS')$avg,
    ylim=c(20,20), xlim=c(16,16), color="#F8766D",size=7
    
  )+
  geom_label_repel(
    arrow = arrow(length = unit(0.015, "npc"), type = "closed", ends = "last"),
    force = 5,
    data=subset(d_filterd,id==31 & avg==subset(d_filterd,id==31 & policy=='w_train')$avg),
    label="w*_train",
    x=31,y=subset(d_filterd,id==31 & policy=='w_train')$avg,
    ylim=c(0,0), xlim=c(30,30), color="#C77CFF",size=7
  )+
  geom_label_repel(
    arrow = arrow(length = unit(0.015, "npc"), type = "closed", ends = "last"),
    force = 5,
    data=subset(d_filterd,id==31 & avg==subset(d_filterd,id==31 & policy=='SAF')$avg),
    label="SAF",
    x=31,y=subset(d_filterd,id==31 & policy=='SAF')$avg,
    ylim=c(20,20), xlim=c(27,27), color="#00BFC4",size=7
  )+
  geom_label_repel(
    arrow = arrow(length = unit(0.015, "npc"), type = "closed", ends = "last"),
    force = 5,
    data=subset(d_filterd,id==41 & avg==subset(d_filterd,id==41 & policy=='w')$avg),
    label="w*",
    x=41,y=subset(d_filterd,id==41 & policy=='w')$avg,

    ylim=c(0,0), xlim=c(41,41), color="#7CAE00",size=7
  )+
  geom_label_repel(
    arrow = arrow(length = unit(0.015, "npc"), type = "closed", ends = "last"),
    force = 5,
    data=subset(d_filterd,id==37 & avg==subset(d_filterd,id==37 & policy=='w_greedy')$avg),
    label="w_greedy",
    x=37,y=subset(d_filterd,id==37 & policy=='w_greedy')$avg,
    ylim=c(20,20), xlim=c(33,33), color="#000000",size=7
  )+

  
  theme_bw()+
  theme(plot.title = element_text(size = 25, face = "bold"),
          axis.text = element_text(size = 15),
          axis.title.x = element_text(size = 25),
          axis.title.y = element_text(size = 25),
        legend.position = "none"
          )+
  xlab("week")+
  ylab("average bounded slowdown")+
  ggsave(paste("images/learned_vs_pure_",save_policies_name,".pdf",sep=""), width=20,height=8)#!
#p+geom_text(data=d_filterd[d_filterd$id==86,],aes(label=policy,x=86,y=policy$avg), hjust = 0.7, vjust = 1)
print(p)

```
## box_plot
```{r fig.width=20 ,fig.height=8}
d=read_trace(performance_path,case=case)
d=d%>%filter(policy %in% c("FCFS","LAFS","SAF","LAF","SPF","LPF","SRF","LRF","SQF","LQF","SEXP","LEXP"))
d
ggplot(data=d)+geom_boxplot(aes(x = reorder(policy, avg, FUN = mean),y=avg))+theme_bw()
   #stat_summary(data = d,fun.y=mean, colour="darkred", geom="point",shape=18, size=3,show_guide = FALSE) 
```
## table of comparaison
```{r fig.width=20 ,fig.height=8}
d=read_trace(performance_path,case=case)
d$policy=recode(d$policy,"past_week"="w_greedy")
d$policy=recode(d$policy,"xnes_dynamic_clairvoyant_3"="w*")
d$policy=recode(d$policy,"learning_offline_3"="w*_train")

d=d%>%filter(policy %in% c("FCFS","LAFS","SAF","LAF","SPF","LPF","SRF","LRF","SQF","LQF","SEXP","LEXP","w*_train","past_week","w*"))
d_train=d %>% filter( id<=1100)
d_test= d %>% filter( id>1100)
d_latex_train=d_train%>%group_by(policy)%>%summarise(average_BSLD=sum(avg))
d_latex_test=d_test%>%group_by(policy)%>%summarise(average_BSLD=sum(avg))
names(d_latex_train)=c("policy","Train")
names(d_latex_test)=c("policy","Test")
#d_latex_train$Test=d_latex_test$Test
d_latex=d_latex_train%>%arrange(Train)
d_latex
print(xtable(d_latex))#,include.rownames=FALSE)
```
## min vs xnes3 vs xnes6
```{r fig.width=20 ,fig.height=8}

df=read_trace(performance_path,case=case)
df$policy=recode(df$policy,"past_week"="w_greedy")
df$policy=recode(df$policy,"xnes_dynamic_clairvoyant_3"="xnes3")
df$policy=recode(df$policy,"xnes_dynamic_clairvoyant_6"="xnes6")
df=df%>%filter(policy %in% c("xnes3","xnes6"))
ggplot(df,aes(x=id,y=avg,group=policy))+
    geom_line(aes(color=policy,linetype=policy))+
      scale_linetype_manual(values=c( min="twodash",xnes3="solid", xnes6="dashed"))+
  scale_color_manual(values=c(min="#000000",xnes3="#F8766D",xnes6="#7CAE00"))+
    scale_x_continuous(breaks=seq(0,100,2))+
    theme_bw()+
    theme(plot.title = element_text(size = 25, face = "bold"),
            axis.text = element_text(size = 15),
            axis.title.x = element_text(size = 25),
            axis.title.y = element_text(size = 25),
            legend.text = element_text(size=25),
            legend.title = element_text(size=25),
            legend.position = "none"
          )+
    geom_label_repel(
      arrow = arrow(length = unit(0.015, "npc"), type = "closed", ends = "last"),
      force = 5,
      data=subset(df,id==43 & avg==subset(df,id==43 & policy=='xnes3')$avg),
      label="xnes3",
      x=43,y=subset(df,id==43 & policy=='xnes3')$avg,
      ylim=c(10,10), xlim=c(43,43), color="#F8766D",size=7
    )+
    geom_label_repel(
      arrow = arrow(length = unit(0.015, "npc"), type = "closed", ends = "last"),
      force = 5,
      data=subset(df,id==43 & avg==subset(df,id==43 & policy=='xnes6')$avg),
      label="xnes6",
      x=43,y=subset(df,id==43 & policy=='xnes6')$avg,
      ylim=c(0,0), xlim=c(41,41), color="#7CAE00",size=7
    )+
    geom_label_repel(
      arrow = arrow(length = unit(0.015, "npc"), type = "closed", ends = "last"),
      force = 5,
      data=subset(df,id==99 & avg==subset(df,id==99 & policy=='min')$avg),
      label="min",
      x=99,y=subset(df,id==99 & policy=='min')$avg,
      ylim=c(30,30), xlim=c(96,96), color="#000000",size=7
    )+
    ggtitle("")+
    xlab("week")+
    ylim(0,15)+
    ylab("average bounded slowdown")+
     ggsave(paste("images/min_vs_xnes3_vs_xnes6_",save_policies_name,".pdf",sep=""), width=20,height=8)
  


```
# CTC-SP2 months
```{r fig.width=20}
d_month=read_trace("data/CTC-SP2/CTC-SP2-Months/policies_performance.csv",case=0)
d_month$policy=recode(d_month$policy,"xnes_dynamic_clairvoyant"="w")
d_month$policy=recode(d_month$policy,"learning_offline"="w_train")
d_min_absolut_month=d_month %>%filter(policy %in% c("SAF","w","w_train","FCFS"))%>%group_by(id)%>% summarise(minimum=min(avg))
d_max_absolut_month=d_month%>%filter(policy %in% c("SAF","w","w_train","FCFS"))%>%group_by(id)%>% summarise(maximum=max(avg))



d_month$mid=(d_max_absolut_month$maximum+d_min_absolut_month$minimum)/2
d_month$width=(d_max_absolut_month$maximum-d_min_absolut_month$minimum)/2


ggplot(d_month%>%filter(policy %in% c("SAF","w","w_train","FCFS")))+
  geom_ribbon(data=d_month,aes(x=id,ymin = mid - width, ymax = mid + width), fill = "grey95") +
  geom_line(aes(x=id,y=avg,col=policy,lty=policy))+
  scale_linetype_manual(values=c( FCFS="dotted",SAF="solid", w="dotted",w_train="solid",w_greedy="solid"))+
  scale_color_manual(values=c(FCFS="#F8766D",w_train="#C77CFF",w_greedy="#000000",SAF="#00BFC4",w="#7CAE00"))+
  theme(axis.text=element_text(size=15),legend.text=element_text(size=15),axis.title=element_text(size=15))+
  geom_rect(mapping = aes(xmin=1,xmax=5.2,ymin=0,ymax=20),alpha=0.0015,color="black",linetype=2,size=0.2)+
  geom_rect(mapping = aes(xmin=5.8,xmax=10,ymin=0,ymax=20),alpha=0.0015,color="black",linetype=2,size=0.2)+
  annotate("text",x=3,y=18,label='atop(bold("Training"))',parse=TRUE,size=7)+
  annotate("text",x=8,y=18,label='atop(bold("Testing"))',parse=TRUE,size=7)+
  scale_x_continuous(breaks=seq(0,10,1))+
  theme(legend.position = "none")+
  # geom_label_repel(
  #   arrow = arrow(length = unit(0.015, "npc"), type = "closed", ends = "last"),
  #   force = 5,
  #   data=subset(d_filterd,id==13 & avg==subset(d_filterd,id==13 & policy=='FCFS')$avg),
  #   label="FCFS",
  #   x=13,y=subset(d_filterd,id==13 & policy=='FCFS')$avg,
  #   ylim=c(20,20), xlim=c(16,16), color="#F8766D",size=7
  #   
  # )+
  # geom_label_repel(
  #   arrow = arrow(length = unit(0.015, "npc"), type = "closed", ends = "last"),
  #   force = 5,
  #   data=subset(d_filterd,id==79 & avg==subset(d_filterd,id==79 & policy=='w_train')$avg),
  #   label="w*_train",
  #   x=79,y=subset(d_filterd,id==79 & policy=='w_train')$avg,
  #   ylim=c(20,20), xlim=c(84,84), color="#C77CFF",size=7
  # )+
  # geom_label_repel(
  #   arrow = arrow(length = unit(0.015, "npc"), type = "closed", ends = "last"),
  #   force = 5,
  #   data=subset(d_filterd,id==39 & avg==subset(d_filterd,id==39 & policy=='SAF')$avg),
  #   label="SAF",
  #   x=39,y=subset(d_filterd,id==39 & policy=='SAF')$avg,
  #   ylim=c(20,20), xlim=c(36,36), color="#00BFC4",size=7
  # )+
  # geom_label_repel(
  #   arrow = arrow(length = unit(0.015, "npc"), type = "closed", ends = "last"),
  #   force = 5,
  #   data=subset(d_filterd,id==86 & avg==subset(d_filterd,id==86 & policy=='w')$avg),
  #   label="w*",
  #   x=86,y=subset(d_filterd,id==86 & policy=='w')$avg,
  # 
  #   ylim=c(0,0), xlim=c(84,84), color="#7CAE00",size=7
  # )+
  
  theme_bw()+
  theme(plot.title = element_text(size = 25, face = "bold"),
          axis.text = element_text(size = 15),
          axis.title.x = element_text(size = 25),
          axis.title.y = element_text(size = 25),
        #legend.position = "none"
          )+
  xlab("month")+
  ylab("average bounded slowdown")+
  ggsave(paste("images/months_comparing_pure_and_learned_heurustics_",save_policies_name,".png",sep=""), width=20,height=8)
```
the the month case the performance is more uniform for CTC-SP2: SAF, w* and w_train give similar bounded slowdown
```{r fig.width=20}
ggplot(d_month)+
  geom_line(aes(x=id,y=avg,col=policy,lty=policy))
d_month %>%group_by(policy) %>% summarise(sum_avg=sum(avg))%>%arrange(sum_avg)
```
## comparing 3 values of thresholding
```{r fig.width=20}
df_thresholding_200000=read.csv("data/thresholding_comparaison/policies_performace_CTC-SP2_200000.csv",header = TRUE)
df_thresholding_none=read.csv("data/thresholding_comparaison/policies_performace_CTC-SP2_604800.csv",header = TRUE)
df_thresholding_72000=read.csv("data/thresholding_comparaison/policies_performace_CTC-SP2_72000.csv",header = TRUE)

df_sum_threshold=df_thresholding_none %>% group_by(policy) %>% summarise(sum_bsld_none=sum(avg))%>% arrange (sum_bsld_none)
df_sum_threshold_200000=df_thresholding_200000 %>% group_by(policy) %>% summarise(sum_bsld_200000=sum(avg))%>% arrange (sum_bsld_200000)
df_sum_threshold_72000=df_thresholding_72000 %>% group_by(policy) %>% summarise(sum_bsld_72000=sum(avg))%>% arrange (sum_bsld_72000)

#df_sum_threshold$sum_bsld_200000=df_thresholding_200000$sum_bsld_200000
#df_sum_threshold$sum_bsld_200000=df_thresholding_200000$sum_bsld_200000
df_sum_threshold$sum_bsld_200000=df_sum_threshold_200000$sum_bsld_200000
df_sum_threshold$sum_bsld_72000=df_sum_threshold_72000$sum_bsld_72000
df_thresholding_none$threshold="None"
df_thresholding_200000$threshold="2,31 days"
df_thresholding_72000$threshold="20 hours"
df_thresholding_all=rbind(df_thresholding_none,df_thresholding_200000,df_thresholding_72000)

fcfs=df_thresholding_none%>%filter(policy=="fcfs")
fcfs$threshold="fcfs"
# ggplot()+
#   geom_line(data = df_thresholding_all%>% filter(policy=="saf" ),aes(x=id,y=avg,color=threshold))+
#   geom_line(data =fcfs,aes(x=id,y=avg,color="fcfs"))

df_thresholding_filtred=rbind(df_thresholding_all%>% filter(policy=="saf" ),fcfs)
ggplot()+
  geom_boxplot(data = df_thresholding_filtred,aes(x=reorder(threshold,avg,mean),y=avg))+
  xlab("threshold")+
  ylab("average bounded slowdown")
  
names(df_sum_threshold)=c("policy","No threshold","2.31 days", "20 hours")
#print(xtable(df_sum_threshold),include.rownames=FALSE)
```
## we admitt that SAF is the best pure policy in general but what we actually advocate in this work is that on a weekly basis picking the best pure policy can has a lot of gain.
```{r}
take_top <- function(d){
  aa=head(d,1)
  return(aa)
}
overall_performance_evaluation<- function( performance_path,name){
  d=read_trace(performance_path,case = case)
  d=d %>% filter(policy %in% c("SAF","LAF","SPF","LPF","FCFS","LCFS","SRF","LRF","SEXP","LEXP","SQF","LQF"))
  d_best=d %>% group_by(id) %>%filter(avg==min(avg))%>%do(take_top(.))
  d_best$policy="Best"
  dd=rbind(d_best%>%group_by(policy)%>%summarise(sum_BSLD=mean(avg)),d%>%group_by(policy)%>%summarise(sum_BSLD=sum(avg)))%>%arrange(sum_BSLD)
  dd$trace=name
  print(dd)
}
overall_performance_evaluation("data/SDSC-SP2/policies_perfomance.csv","SDSC-SP2")
overall_performance_evaluation("data/SDSC-BLUE/policies_performace.csv","SDSC-BLUE")
overall_performance_evaluation("data/KTH-SP2/policies_performace.csv","KTH-SP2")
overall_performance_evaluation("data/CTC-SP2/policies_performace.csv","CTC-SP2")
overall_performance_evaluation("data/ANL-Intr/policies_performace.csv","ANL-Intr")
overall_performance_evaluation("/home/salah/expriements/workflow_bbo/mixed-policies/workflow/logs/Egee/policies_performace.csv","Egee")
overall_performance_evaluation("/home/salah/expriements/workflow_bbo/mixed-policies/workflow/logs/Sandia/policies_performace.csv","Sandia")
overall_performance_evaluation("/home/salah/expriements/workflow_bbo/mixed-policies/workflow/logs/Gaia/policies_performace.csv","Gaia")
overall_performance_evaluation("/home/salah/expriements/workflow_bbo/mixed-policies/workflow/logs/CEA-Curie/policies_performace.csv","CEA-Curie")
```

